/**
 * Copyright(c)2013-2014, Rinc Liu(http://rincliu.com).
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rincliu.library.widget.pulltorefresh;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.rincliu.library.R;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public abstract class PullToRefreshAbsView extends LinearLayout implements OnTouchListener {
    public static final int STATUS_PULL_TO_REFRESH = 0x1111;

    public static final int STATUS_RELEASE_TO_REFRESH = 0x3333;

    public static final int STATUS_REFRESHING = 0x5555;

    public static final int STATUS_REFRESH_FINISHED = 0x9999;

    public static final int SCROLL_SPEED = -20;

    private float yDown;

    private int headerOffset;

    private int currentStatus = STATUS_REFRESH_FINISHED;

    private int lastStatus = STATUS_REFRESH_FINISHED;

    private int touchSlop;

    private boolean hasInitHeaderOffset;

    private boolean isReadyToPull;

    private Context context;

    private ViewGroup contentView;

    private View header;

    private ProgressBar pb;

    private ImageView iv_arrow;

    private TextView tv_desc, tv_time;

    private MarginLayoutParams headerLayoutParams;

    private PullToRefreshListener onRefreshListener;

    /**
     * @param context
     */
    public PullToRefreshAbsView(Context context) {
        super(context);
        init(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public PullToRefreshAbsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        header = LayoutInflater.from(context).inflate(R.layout.pull_to_refresh_header, null, true);
        pb = (ProgressBar) header.findViewById(R.id.pb);
        iv_arrow = (ImageView) header.findViewById(R.id.iv_arrow);
        tv_desc = (TextView) header.findViewById(R.id.tv_desc);
        tv_time = (TextView) header.findViewById(R.id.tv_time);
        touchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        setOrientation(VERTICAL);
        addView(header, 0);
    }

    /**
	 * 
	 */
    protected void addContentView() {
        contentView = onCreateContentView();
        contentView.setOnTouchListener(this);
        addView(contentView, 1);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (changed && !hasInitHeaderOffset) {
            headerOffset = -header.getHeight();
            headerLayoutParams = (MarginLayoutParams) header.getLayoutParams();
            headerLayoutParams.topMargin = headerOffset;
            hasInitHeaderOffset = true;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        checkPull(event);
        if (isReadyToPull) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    yDown = event.getRawY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    float yMove = event.getRawY();
                    int distance = (int) (yMove - yDown);
                    if (distance <= 0 && headerLayoutParams.topMargin <= headerOffset) {
                        return false;
                    }
                    if (distance < touchSlop) {
                        return false;
                    }
                    if (currentStatus != STATUS_REFRESHING) {
                        if (headerLayoutParams.topMargin > 0) {
                            currentStatus = STATUS_RELEASE_TO_REFRESH;
                        } else {
                            currentStatus = STATUS_PULL_TO_REFRESH;
                        }
                        headerLayoutParams.topMargin = (distance / 2) + headerOffset;
                        header.setLayoutParams(headerLayoutParams);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                default:
                    if (currentStatus == STATUS_RELEASE_TO_REFRESH) {
                        new RefreshTask().execute();
                    } else if (currentStatus == STATUS_PULL_TO_REFRESH) {
                        new HideHeaderTask().execute();
                    }
                    break;
            }
            if (currentStatus == STATUS_PULL_TO_REFRESH || currentStatus == STATUS_RELEASE_TO_REFRESH) {
                updateHeaderView();
                contentView.setPressed(false);
                contentView.setFocusable(false);// TODO
                // listView.setFocusableInTouchMode(false);
                lastStatus = currentStatus;
                return true;
            }
        }
        return false;
    }

    public interface PullToRefreshListener {
        void onRefresh();
    }

    /**
     * @param onRefreshListener
     */
    public void setOnRefreshListener(PullToRefreshListener onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
    }

    /**
	 * 
	 */
    public void finishRefreshing() {
        currentStatus = STATUS_REFRESH_FINISHED;
        new HideHeaderTask().execute();
        getHandler().post(new Runnable() {
            @Override
            public void run() {
                tv_time.setVisibility(View.VISIBLE);
                tv_time.setText(context.getString(R.string.ptr_updated_at)
                        + " "
                        + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA).format(new Date(
                                System.currentTimeMillis())));
            }
        });
    }

    /**
     * @return
     */
    public abstract ViewGroup onCreateContentView();

    /**
     * @return
     */
    public abstract boolean onCheckIsTop();

    private void checkPull(MotionEvent event) {
        View firstChild = contentView.getChildAt(0);
        if (firstChild != null) {
            if (onCheckIsTop()) {
                if (!isReadyToPull) {
                    yDown = event.getRawY();
                }
                isReadyToPull = true;
            } else {
                if (headerLayoutParams.topMargin != headerOffset) {
                    headerLayoutParams.topMargin = headerOffset;
                    header.setLayoutParams(headerLayoutParams);
                }
                isReadyToPull = false;
            }
        } else {
            isReadyToPull = true;
        }
    }

    private void updateHeaderView() {
        if (lastStatus != currentStatus) {
            if (currentStatus == STATUS_PULL_TO_REFRESH) {
                tv_desc.setText(getResources().getString(R.string.ptr_pull_to_refresh));
                iv_arrow.setVisibility(View.VISIBLE);
                pb.setVisibility(View.GONE);
                rotateArrow();
            } else if (currentStatus == STATUS_RELEASE_TO_REFRESH) {
                tv_desc.setText(getResources().getString(R.string.ptr_release_to_refresh));
                iv_arrow.setVisibility(View.VISIBLE);
                pb.setVisibility(View.GONE);
                rotateArrow();
            } else if (currentStatus == STATUS_REFRESHING) {
                tv_desc.setText(getResources().getString(R.string.ptr_refreshing));
                pb.setVisibility(View.VISIBLE);
                iv_arrow.clearAnimation();
                iv_arrow.setVisibility(View.GONE);
            }
        }
    }

    private void rotateArrow() {
        float pivotX = iv_arrow.getWidth() / 2f;
        float pivotY = iv_arrow.getHeight() / 2f;
        float fromDegrees = 0f;
        float toDegrees = 0f;
        if (currentStatus == STATUS_PULL_TO_REFRESH) {
            fromDegrees = 180f;
            toDegrees = 360f;
        } else if (currentStatus == STATUS_RELEASE_TO_REFRESH) {
            fromDegrees = 0f;
            toDegrees = 180f;
        }
        RotateAnimation animation = new RotateAnimation(fromDegrees, toDegrees, pivotX, pivotY);
        animation.setDuration(100);
        animation.setFillAfter(true);
        iv_arrow.startAnimation(animation);
    }

    private class RefreshTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            int topMargin = headerLayoutParams.topMargin;
            while (true) {
                topMargin = topMargin + SCROLL_SPEED;
                if (topMargin <= 0) {
                    topMargin = 0;
                    break;
                }
                publishProgress(topMargin);
            }
            currentStatus = STATUS_REFRESHING;
            publishProgress(0);
            if (onRefreshListener != null) {
                onRefreshListener.onRefresh();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... topMargin) {
            updateHeaderView();
            headerLayoutParams.topMargin = topMargin[0];
            header.setLayoutParams(headerLayoutParams);
        }
    }

    private class HideHeaderTask extends AsyncTask<Void, Integer, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {
            int topMargin = headerLayoutParams.topMargin;
            while (true) {
                topMargin = topMargin + SCROLL_SPEED;
                if (topMargin <= headerOffset) {
                    topMargin = headerOffset;
                    break;
                }
                publishProgress(topMargin);
            }
            return topMargin;
        }

        @Override
        protected void onProgressUpdate(Integer... topMargin) {
            headerLayoutParams.topMargin = topMargin[0];
            header.setLayoutParams(headerLayoutParams);
        }

        @Override
        protected void onPostExecute(Integer topMargin) {
            headerLayoutParams.topMargin = topMargin;
            header.setLayoutParams(headerLayoutParams);
            currentStatus = STATUS_REFRESH_FINISHED;
        }
    }
}
