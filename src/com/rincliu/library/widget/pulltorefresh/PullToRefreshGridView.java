/**
 * Copyright (c) 2013-2014, Rinc Liu (http://rincliu.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rincliu.library.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class PullToRefreshGridView extends PullToRefreshAbsView {

    /**
     * @param context
     */
    public PullToRefreshGridView(Context context) {
        super(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public PullToRefreshGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private GridView gridView;

    /**
     * @param gridView
     */
    public void setContentView(GridView gridView) {
        this.gridView = gridView;
        super.addContentView();
    }

    @Override
    public ViewGroup onCreateContentView() {
        if (gridView == null) {
            throw new NullPointerException("Have you called setContentView?");
        }
        return gridView;
    }

    @Override
    public boolean onCheckIsTop() {
        boolean res = false;
        View firstChild = gridView.getChildAt(0);
        if (firstChild != null) {
            int firstVisiblePos = gridView.getFirstVisiblePosition();
            if (firstVisiblePos == 0 && firstChild.getTop() == 0) {
                res = true;
            }
        }
        return res;
    }
}
