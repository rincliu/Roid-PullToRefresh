/**
 * Copyright (c) 2013-2014, Rinc Liu (http://rincliu.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rincliu.library.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ScrollView;

public class PullToRefreshScrollView extends PullToRefreshAbsView {

    /**
     * @param context
     */
    public PullToRefreshScrollView(Context context) {
        super(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public PullToRefreshScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private ScrollView scrollView;

    /**
     * @param scrollView
     */
    public void setContentView(ScrollView scrollView) {
        this.scrollView = scrollView;
        super.addContentView();
    }

    @Override
    public ViewGroup onCreateContentView() {
        if (scrollView == null) {
            throw new NullPointerException("Have you called setContentView?");
        }
        return scrollView;
    }

    @Override
    public boolean onCheckIsTop() {
        return scrollView.getScrollY() <= 0;
    }
}
